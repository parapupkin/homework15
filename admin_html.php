 <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <link rel="stylesheet" type="text/css" href="style.css"> 
    </head>
    <body> 
        <div class="container_admin">         
            <?php if (isset($tableData)) { ?>
            <h2>Вы просматриваете данные о полях таблицы <i><?= $tableName ?></i></h2>         
            <table class="admin_table">
                <tr>
                    <?php 
                        $result = [];
                        foreach($tableData as $sub) {
                            $result = array_merge($result, $sub);                  
                        }                                  
                            foreach ($result as $key => $value) { ?>  
                    <th><?= $key ?></th>
                        <?php }  ?> 
                    <th>Изменить/Удалить</th>                 
                </tr>
                    <?php                
                        foreach ($tableData as $value) { ?>
                <tr>
                        <?php foreach ($value as $key => $data) {  
                            $tableResultData[$key] = $data;   ?>                      
                    <td>
                        <?= $data ?>
                    </td>                    
                        <?php } ?>
                    <td>
                        <form action="index.php" method="POST">
                            <input type="text" name="new_name" placeholder="Новое название поля">
                            <input type="hidden" name="table" value="<?php echo $tableName ?>">
                            <input type="hidden" name="field" value="<?php echo $tableResultData['Field'] ?>">
                            <input type="hidden" name="type" value="<?php echo $tableResultData['Type'] ?>">
                            <input type="submit" value="Изменить имя поля">                                       
                        </form>
                        <form action="index.php" method="POST"> 
                            <select name="change_type">
                                <option>int(11)</option>
                                <option>float</option>
                                <option>text</option> 
                                <option>varchar(150)</option>
                                <option>tinyint(4)</option>                                                               
                            </select>                           
                            <input type="hidden" name="table" value="<?php echo $tableName ?>">
                            <input type="hidden" name="field" value="<?php echo $tableResultData['Field'] ?>">                                                     
                            <input type="submit" value="Изменить тип">                                        
                        </form>    
                        <form action="index.php" method="POST">                            
                            <input type="hidden" name="table" value="<?php echo $tableName ?>">
                            <input type="hidden" name="delete" value="<?php echo $tableResultData['Field'] ?>">                            
                            <input type="submit" action name="" value="Удалить поле">                                        
                        </form>                    
                    </td>       
                </tr>
                <?php } ?>                
            </table>
        </div>            
        <?php } else { ?>
        <div class="container_admin">             
            <h2>Добро пожаловать</h2>          
            <p>При запуске Вами этого файла произошли следующие события:</p>
            <p><?= $message ?>,</p>
            <p><?= $message_row ?></p>            
            <p><b>Вы можете просмотреть следующие таблицы в этой базе данных:</b></p>
            <table class="admin_table">
                <tr>
                    <th>№ п/п</th>
                    <th>Название таблицы</th>
                    <th>Перейти к просмотру таблицы</th>                     
                </tr>
                    <?php 
                    $i = 1;    
                    foreach ($tablesName as $value) {
                        foreach ($value as $tableName) {                       
                    ?>                      
                <tr>              
                    <td>
                        <?php echo $i ?>                        
                    </td>                
                    <td>                        
                        <?php echo $tableName ?>                        
                    </td>
                    <td>
                        <form action="index.php" method="POST">
                            <input type="hidden" name="table" value="<?php echo $tableName ?>">
                            <input type="submit"  name="" value="Перейти к <?php echo $tableName ?>">
                        </form>                       
                    </td>                
                    <?php
                         $i++;
                         }
                    }           
                    ?>                    
                </tr>
            </table>
            <p> <b>p.s. издевайтесь, пожалуйста, только над таблицей cabinet_parts</b> </p>              
                    <?php } 
            if (isset($tableData)) { ?>
            <ul class="container_admin">
                <li><a href="index.php" >Вернуться к списку таблиц</a></li>            
            </ul>
            <?php } ?>
        </div>      
    </body>
    </html>