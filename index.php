﻿<?php
    
    // Функция для проверки данных, полученных от пользователя
    $pattern = "/^[0-9a-z_]+$/i";
    $patternType = "/^[0-9a-z()]+$/i";
    function check_data($pattern, $value_user) 
    {        
        if (!preg_match($pattern, $value_user)) {
            exit("Допустимо использовать только латинские буквы, цифры и символ нижнего подчеркивания!");
        }        
    }    

    $host = 'localhost';
    $dbname = 'homeworks';
    $user = 'root';
    $pass = '';

    try {
        $db = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $user, $pass);
         
        // если пользователь запросил данные по таблице, то работаем с ней
        if (isset($_POST['table'])) {
            check_data($pattern, $_POST['table']); 

            // если имеются данные о каком-либо поле - выясняем, что нужно сделать (переименовать или изменить тип) и выполняем это
            if (isset($_POST['field'])) {
                check_data($pattern, $_POST['field']); 
                if (isset($_POST['change_type'])) {
                    check_data($patternType, $_POST['change_type']);
                    $change_type = $db->prepare("ALTER TABLE $_POST[table] MODIFY $_POST[field] $_POST[change_type] NOT NULL")->execute(); 
                }
                if (isset($_POST['new_name']) && isset($_POST['type'])) {
                    check_data($pattern, $_POST['new_name']);
                    check_data($patternType, $_POST['type']);
                    $sql_rename = $db->prepare("ALTER TABLE $_POST[table] CHANGE $_POST[field] $_POST[new_name] $_POST[type]")->execute();    
                }
            }
           
            // если в запрошенных данных имеется запрос об удалении, удаляем указанное поле
            if (isset($_POST['delete'])) { 
                check_data($pattern, $_POST['delete']);               
                $sql_delete = $db->prepare("ALTER TABLE $_POST[table] DROP $_POST[delete]")->execute();               
            } 
            
            // помещаем название таблицы в переменную для дальнейшей работы
            $tableName = $_POST['table']; 
            // выводим данные о полях таблицы          
            $sqlDescribe = $db->prepare("DESCRIBE $tableName");  
            $sqlDescribe->execute();          
            $tableData = $sqlDescribe->fetchAll(PDO :: FETCH_ASSOC);

        } else {
            // если пользователь не запрашивал данные по таблице - создаем ее и пару строк в ней
            $sqlCreate = "
                DROP TABLE IF EXISTS `cabinet_parts`;
            
                CREATE TABLE `cabinet_parts` (
                    `id` INT(11) NOT NULL AUTO_INCREMENT,
                    `name_detail` VARCHAR(150) NOT NULL,
                    `numbery` TINYINT NOT NULL,
                    PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
            ";
            $db->prepare($sqlCreate)->execute() ? $message = '- таблица cabinet_parts создана' : $message = '- ошибка при создании таблицы';
            $sqlInsert = "INSERT INTO `cabinet_parts` (`name_detail`, `numbery`) VALUES ('ручка', '4');";
            $db->prepare($sqlInsert)->execute() ? $message_row = '- в таблицу cabinet_parts успешно добавлена запись' : $message_row = ' - ошибка при добавлении записи';
            // получаем имена всех таблиц, имеющихся в бд
            $sglShowTables = $db->query("SHOW TABLES");
            $tablesName = $sglShowTables->fetchAll(PDO :: FETCH_ASSOC);
        }           

    } catch (Exception $e) {
        die('Error: ' . $e->getMessage() . '<br/>');
    }

    require_once'admin_html.php'; 

    // Создать новую таблицу через php.
    // Сделать страницу, где будет выводиться список таблиц текущей базы данных. В каждую таблицу можно зайти и увидеть название и тип поля.
    // Добавить возможность удалить поле, изменить его тип или название.